# Poème en alexandrins pour raconter un voyage en Écosse

## Le poème
- [Le poème, joliment mis en page en PDF](https://perso.crans.org/besson/publis/Ecosse_2018_en_alexandrins/ecosse.pdf),
- [Le poème, en version web réactive et avec ma police manuscrite](https://perso.crans.org/besson/publis/Ecosse_2018_en_alexandrins/ecosse.pdf).

## Aperçu
![](screenshots/ecosse_pdf.png)

![](screenshots/ecosse_html.png)

## Les sources
- [Le poème en Markdown, non mis en page](ecosse.md),
- [Le poème en version web, mis en page](index.html),
- Le poème en LaTeX, mis en page : [squelette](latex/main.tex), [classe](latex/tufte-book.cls), et [poème](latex/Ecosse.tex).

## [Auteurs](AUTHORS)
- [Lilian Besson](https://bitbucket.org/lbesson/),
- Sélim Cornet,
- Angèle Fouquet,
- Clément Alapetite.

## [Licence](LICENSE)
Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License

Copyright (C) 2018 Lilian Besson (Naereen), and friends, https://bitbucket.org/lbesson/
