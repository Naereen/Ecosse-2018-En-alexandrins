# Voyage en Écosse
- Par Lilian, Sélim, Angèle et Clément.
- À Hélène, que l'on aurait aimé avoir avec nous.


## Prologue

L'idée de l'échappée, en Écosse naquit  
en l'an deux mille dix huit, au mois de février,  
lors d'une froide journée, partagée entre amis,  
en plein cœur de l'hiver. Ô quelle chanceuse idée !

Laissez nous vous compter, par quelques mots rimant,  
notre belle épopée, d'un si plaisant voyage.  
Pardonnez nos erreurs, nous sommes encor enfants,  
dans cet art délicat, du poétique lettrage.


## Vendredi 27

Nous partîmes tout seuls, et par un prompt renfort,  
Arrivâmes tous quatre, au grand aéroport !  
Un vol à Édimbourg, prestement nous porta.  
Pour enfin recevoir, la chignole qu'on loua.

Une grosse barrique, bien confortable et chic,  
elle nous mena bien vite, vers un tardif pic-nic.

Premier soir en plein champ, la tente fut montée.  
Ni vaches ni tracteurs, rien ne nous dérangea,  
ni même l'escalade de deux vieux barbelés.  
Derrière de gros nuages, la lune se cacha.

L'éclipse fut ratée, pas grave mais dommage,  
et nous dormîmes au son d'un menaçant orage.


## Samedi 28

Au hasard d'un chemin, abîmé par le vent,  
trouvailles d'un cerf-volant, sale, vieux, rouge et blanc.  
Un peu de nettoyage, et le voilà filant !  
Nous le fîmes voler, tous quatre un bon moment !

Du jardin d'Abroath, nous vîmes l'abbaye !  
Ruine romantique, pareille à Pompéi.

Le premier déjeuner, joie d'une vue sur mer,  
pour un délicieux haddock en fish and chips,  
avec sa mayonnaise, nul besoin de dessert.  
Répit bien au soleil, avant l'apocalypse !

Un peu de route, et à un lac nous voilà,  
réserve d'oiseaux : des oies, il y en eut des tas.


## Dimanche 29

Après une nuit humide, nous nous réveillâmes  
sous d'immenses sapins, symbole du Glen Clova,  
ô grandiose vallée, d'à peine cent dix âmes.  
Des brumes et des moutons, nous en restâmes coi !

Une heure d'ascension, sous une forte pluie,  
pour le frisson d'atteindre, le petit Loch Brandy.

Après le froid et l'effort, une pause au chaud,  
confort d'une auberge, nous ne manquâmes de rien,  
Deux bons chocolats chauds, un Americano,  
une pinte de lager : tout cela fit du bien.

Pause à Kirriemuir, un village fort charmant,  
Jolie place amusante, rencontre de Peter Pan !

Conduite si angoissante, sur la route embrumée,  
que des vers éclairés, seuls purent dissiper.  
Cap droit sur le nord, vers le port d'Aberdeen,  
Merci à CouchSurfing, pour l'accueil de Kevin !

Diner de ratatouille, ananas et whisky.  
De Colons de Catane, une traître partie.


## Lundi 30

Suivant les bons conseils, de ce charmant Kevin,  
pour voir bien installée, une colonie de phoques,  
et d'oiseaux sauvages, et une brise marine,  
nous partîmes fort tôt, presqu'au réveil du coq…

Vers le nord à Newburgh, et pour notre bonheur,  
le rare soleil enfin, nous nourrit de chaleur.

Après un héroïque et relaxant moment,  
s'entraînant face au vent, à jouer du cerf-volant,  
l'air du grand large aida, nous pûmes déjeuner…  
Un doux cidre bien frais, un très bon taboulé !

Elle fut si déchaînée, coup de cœur pour la mer,  
aux ruines du château, la muse de Bram Stoker.

Sur la route de Pennan, nous continuâmes  
pittoresque village, demeures de pêcheurs,  
aux pieds de falaises, que nous quatre évitâmes,  
en garant le carrosse, avant leurs folles hauteurs.

Nuit trempée sous le vent, près de restes gothiques :  
église abandonnée, cimetière celtique.


## Mardi 31

Le secret des Grampians, c'est leur précieux whisky,  
Épanchons nos soifs, sus à la distillerie !  
Cuve d'orges maltés, bâtisses de granit,  
alambics à vapeur, et chaleur qui palpite,

A une heure tardive, un arrêt en forêt,  
pour un bon déjeuner, ou bien fut-ce le goûter ?

Miracle de confort, ce soir fini le froid,  
au camping nous dormîmes, ô combien mieux qu'hier !  
Malchance aux pauvres Sélim, et Angèle les deux proies  
de tiques maléfiques, petites et forestières.

Pour chasser cet instant, nous voilà partis marchant,  
lapins, vaches et moutons, broutant tranquillement.

Un savoureux dîner, à table à Charlestown :  
saumon, haddock fumé, macérés au whisky,  
accra d'haggis panné, et burger au bacon…  
Citons Sire Karadoc, “Le gras, c'est la vie” !

Morphée nous accueilli, nous dormîmes bien vite,  
nos bedaines remplies, de viandes et de frites.


## Mercredi 1

Douceur de cette nuit, nos panses bien tendues,  
le délicieux repas, de la veille nous aida,  
à dormir paisiblement, nous quatre repus.  
Un courageux duo courut mille pas.

Prenons des forces, ô plaisir quotidien  
du petit déjeuner, initiant le matin.

Visite d'une antique fabrique de tonneaux,  
qui par millier restaure, des milliers de barriques,  
“Speyside cooperage”, qui abat du bouleau.  
Hommes rudes à la tâche, guide clair et typique.

Emplettes gourmandes au petit magasin :  
à l'usine Walkers les biscuits donnent faim.

Pour esquiver la nuit, trop froides et mouillées,  
chez l'Acadien Vincent, nous fûmes hébergés.  
Un tajine bien épicé comme délicieux dîner,  
légumes et couscous, abricots et poulet…

Soirée enrichissante, s'échangeant des histoires,  
ce fut une bonne manière de se dire bonsoir.


## Jeudi 2

Réveil trop matinal, départ en randonnée,  
pour Lilian et Sélim, qui voulurent marcher.  
Trois heures si belles, Kingussie — Newtonmore,  
loch brumeux et sentiers, moutons sentant très forts !

Pour Angèle et Clément, visite d'un musée  
folklorique attraction, ils y furent amusés.

Longs paysages lunaires, quasi désertiques,  
qu'une route serpentine notre souffle coupa…  
Tant de miles et de yards, parcourus en musique :  
Sting, Matmatah, Renaud, Brel et Metallica…

Ce soir à Stirling, le fier château royal  
nous accueillit austère, quelle fierté nationale.


## Vendredi 3

Enfin nous visâmes, pour notre dernière nuit,  
la capitale d'Écosse, la noble Édimbourg.  
Repos bien au sec dans une verte prairie,  
un copieux déjeuner nous souhaita le bonjour.

Au musée national, un long bain de culture,  
de sciences et techniques, merveilles de la nature.

Tous passionnés d'Histoire, mais autant de cuisine,  
nous voulûmes finir bien-sûr par un repas.  
Dans une rue antique, aux goûts et saveurs fines,  
patates cuites au four, dont on se délecta.

Coûteux mais nécessaire, unique remplissage,  
du fidèle destrier, qui permit ce voyage.

Visant l'aéroport, notre large carrosse fila,  
derniers miles au voulant, nous finîmes par marcher.  
Nous comptâmes sans tarder toutes ces livres dépensées,  
car ce sont les bons comptes, qui font les bons amis.

Un vol sans encombre nous mena à Paris.  
Triste séparation, mais ce voyage nous ravit !


## Épilogue

Remerciements sincères, à tous ces vieux héros,  
à William Wallace, le guerrier et sauveur,  
aussi à Walter Scott, le noble romancier,  
Sean Connery, qui des James Bond fut le plus beau,  
Mélinda la rebelle, à la noble rousseur…  
Vers la belle Écosse, tous nous ont attirés.


Concernant la méthode, pour ces alexandrins,  
chaque jour du voyage, pour raconter la veille,  
Lilian propose une trame, que peaufinent les autres.  
Si ce style vous plaît, si ces sons là sont fins,  
remerciez nos cervelles, qui telles des abeilles  
ont butiné ces mots, maintenant dev'nus vôtres.


Dédicace finale, à tous nos compagnons,  
Lollypop notre auto, qui fut d'un grand confort,  
Fujivan le cerf-volant, maniable et léger.  
Kevin à Aberdeen, et sa grande maison,  
Vincent à Kingussie, et son accent si fort,  
et tous ces écossais, gentils et bien élevés !
